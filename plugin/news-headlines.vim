"  vim-news-headlines
"  --------------
"  Get news top headlines inside Vim using https://newsapi.org
"
"  Author:  skamsie <alexandru.dimian@gmail.com>
"  Website: https://github.com/skamsie/vim-headlines
"  License: MIT (see LICENSE file)


" Filetype plugins need to be enabled
filetype plugin on

" Load ftplugin when opening .news-headlines buffer
au! BufRead,BufNewFile *.news-headlines set filetype=news-headlines

" Set required defaults
if !exists("g:news_headlines_sources")
  let g:news_headlines_sources = 'cnn,bbc-news,wired'
endif

if !exists("g:news_headlines_topics")
  let g:news_headlines_topics = ''
endif

if !exists("g:news_headlines_wrap_text")
  let g:news_headlines_wrap_text = 120
endif

if !exists("g:news_headlines_show_published_at")
  let g:news_headlines_show_published_at = 1
endif

function! NewsHeadlines(...)
  setlocal modifiable
  execute "edit .news-headlines"
  normal! gg
  setlocal noma
endfunction

command! NewsHeadlines call NewsHeadlines()
