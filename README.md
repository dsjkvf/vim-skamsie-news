vim-skamsie-news
================

## About

This is a fork of [vim-headlines-news](https://github.com/skamsie/vim-news-headlines) plugin by [Skamsie](https://github.com/skamsie) with a few bugs fixed and some features added. However, I strongly encourage you to use the original plugin, and naturally, all the credits go to the original author.

## Changes

  - Bugfixes include better topics collection, better date sorting, better duplicates removal, and some others;
  - Added features include the following:
    - all news, both from [top headlines](https://newsapi.org/docs/endpoints/top-headlines) and ["everything else"](https://newsapi.org/docs/endpoints/everything) topics are now limited to two days;
    - support for `domains` in [topics](https://newsapi.org/docs/endpoints/everything) added: you are now able to define `g:news_headlines_topics` like this: `{'topic': 'apple', 'language': 'en', sorted_by: 'date', 'domains': 'wsj.com'}`;
    - some shortcuts introduced:
        - `j/k` to go to the next / previous article;
        - `J/K` to go to the next / previous source;
    - besides, the NewsAPI key is now stored alongside other plugin's settings under variable `g:news_headlines_API`.

## Hints

  - Headlines from sources defined in `g:news_headlines_sources` will be listed as entered;
  - Topics from queries specified in `g:news_headlines_topics` will be listed alphabetically. However, if one seeks to introduce some certain distinction between different groups of topics, it's worth to note that the search performed for [topics](https://newsapi.org/docs/endpoints/everything) is case-insensitive, and while uppercased and lowercased queries would produce the same results, these results will be sorted differently. Therefore, in this example the results on Microsoft will be listed above the results on bitcoin:

        let g:news_headlines_topics =
              \ [
              \   {'topic': 'bitcoin', 'domains': 'lifehacker.com'},
              \   {'topic': 'Microsoft', 'language': 'en', 'domains': 'wsj.com'}
              \ ]

  - The plugin also plays nicely with [junegunn's](https://github.com/junegunn/) [limelight.vim](https://github.com/junegunn/limelight.vim), and if the latter is installed, it will be launched automatically upon entering the `news-headlines` buffer;
  - The plugin can also preview articles within Vim [thanks](https://github.com/jarun/googler/wiki/Terminal-Reading-Mode-or-Reader-View#using-reader) to [reader.py](https://github.com/zyocum/reader) -- and if the script is installed, pressing `p` on an article will open it in a split `news-headlines-preview` buffer;
  - And finally, while being in that buffer, pressing `s` will share the article in question on Twitter (by default, this sharing mechanism uses macOS's `open` command, which can be adapted by editing `news-headlines-preview.vim` file from `ftplugin` folder.

## Disclaimer

The name of the plugin reflects the contribution of the original [author](https://github.com/skamsie) and the initial code the plugin still shares. Which may or may not change in the future.
