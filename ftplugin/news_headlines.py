# -*- coding: utf-8 -*-

# Modules
from __future__ import print_function, division
import subprocess
import json
import sys
import textwrap
import vim
import webbrowser
import collections
import threading
import re
from difflib import get_close_matches
from datetime import datetime, timedelta
if sys.version_info >= (3, 0):
    from html.parser import HTMLParser
    from urllib.request import urlopen
    from urllib.parse import urlencode
    unicode = bytes
    unichr = chr
else:
    from HTMLParser import HTMLParser
    from urllib2 import urlopen
    from urllib import urlencode


# Settings
API_KEY = vim.eval('g:news_headlines_API')
SOURCE_FOLDS = collections.OrderedDict()
SOURCE_EMPHASIS = '⁕⁕⁕'
TITLE_EMPHASIS = '> '
GROUPS = []  # sources, topics
MENU = []  # menu items


# Mechanism
class GetNews:
    def __init__(self, api_key, with_threading=True):
        self.api_key = api_key
        self.with_threading = with_threading
        self.api_base_url = 'https://newsapi.org/v2'

    def _sort_by_date(self, l, limit_by_date = True):
        """ Try to sort by date, recent first """
        if not l: return l
        # catch all non-defined dates and replace those with Null
        for i in l:
            try:
                the_day = datetime.strptime(str(i['publishedAt']), "%Y-%m-%dT%H:%M:%SZ").date()
                if limit_by_date:
                    the_lim = datetime.utcnow().date() - timedelta(days = 1)
                    if the_day < the_lim:
                        i['publishedAt'] = 'deadcat'
            except ValueError:
                i['publishedAt'] = ''
        # now, sort
        sorted_l = sorted(l,
                key=lambda x: x['publishedAt'] if x['publishedAt'] else '',
                reverse=True)
        # but remove entries older than two days
        sorted_l = [i for i in sorted_l if not ('deadcat' in i['publishedAt'])]
        return sorted_l

    def _cleanup_duplicates(self, l):
        """ Use diff sequences to exclude similar topics """
        if not l: return l
        clean_l = []
        while l:
            # (1) remove an item from the list, (2) add it to the new list, (3) and then rebuilt the initial list
            # in such a manner that it will contain no entries similar to this removed item
            topic = l.pop(0)
            clean_l.append(topic)
            l = [x for x in l if x['title'] not in get_close_matches(topic['title'], (z['title'] for z in l), cutoff=0.7)]
        return clean_l

    def get_sources(self, filters=None):
        """Get news sources.
        filters: <dict> filter the response

        documentation:
          https://newsapi.org/docs/endpoints/sources
        """
        url = '%s/sources?apiKey=%s' % (self.api_base_url, self.api_key)
        response = json.loads(urlopen(url, timeout=5).read().decode('utf-8'))
        sources = response['sources']
        if filters:
            sources = list(filter(
                lambda x: all(x[y] == z for y, z in filters.items()), sources))
        return sources

    def get_top_headlines(self, sources):
        """Get top headlines.
        sources: <str> 'bbc-news, cnn, ...'

        documentation:
          https://newsapi.org/docs/endpoints/top-headlines
        """
        url = '%s/top-headlines?apiKey=%s&sources=%s' % (self.api_base_url,
                                                         self.api_key, sources)
        response = json.loads(urlopen(url, timeout=5).read().decode('utf-8'))
        return response['articles']

    def get_by_topic(self, topic, domains='', language='', sort_by=None):
        """Get headlines by topic."""
        if not sort_by:
            sort_by = 'relevance'
        _from = datetime.utcnow().date() - timedelta(days = 1)
        params = {
            'apiKey': self.api_key, 'q': topic,
            'language': language, 'sortBy': sort_by,
            'from': _from,
            'domains': domains
        }
        url = '%s/everything?%s' % (self.api_base_url, urlencode(params))
        response = json.loads(urlopen(url, timeout=5).read().decode('utf-8'))
        return response['articles']

    def group_by_source(self, sources, sorted_by_date=True, limit_at=False):
        """Parse headlines and group them by source
        sources: <str> 'bbc-news, cnn, ...'
        return: <dict>
        """
        sources = sources.replace(' ', '')
        headlines = self.get_top_headlines(sources)
        grouped_by_source = collections.OrderedDict()

        for s in sources.split(','):
            grouped_by_source[s] = []
        for headline in headlines:
            if headline['title'] and headline['url']:
                source = headline['source']['id']
                if source in grouped_by_source.keys():
                    grouped_by_source[source].append(headline)
        if sorted_by_date:
            for k, v in grouped_by_source.items():
                grouped_by_source[k] = self._sort_by_date(v)
        if limit_at:
            for k, v in grouped_by_source.items():
                if v: grouped_by_source[k] = v[:limit_at]
        return grouped_by_source

    def group_by_topic(self, topics, sorted_by_date=True, limit_at=15):
        """Parse headlines and group them by topic
        topics: <list> [{'topic': 'apple', 'language': 'en'}, {'topic': ...}]
        limit_at: remove items until this number
        return: <dict>
        """
        grouped_by_topic = collections.OrderedDict()

        def dict_updater(topic, headlines):
            for headline in headlines:
                if headline['title'] and headline['url']:
                    if topic in grouped_by_topic.keys():
                        grouped_by_topic[topic].append(headline)
                    else:
                        grouped_by_topic[topic] = []
                        grouped_by_topic[topic].append(headline)

        def send_request(topic):
            if ('sort_by' in topic.keys()) and topic['sort_by']:
                _sort_by = topic['sort_by']
            else:
                _sort_by = None
            if ('domains' in topic.keys()) and (topic['domains']) and ('language' in topic.keys()) and (topic['language']):
                topic_name = '%s(%s)' % (topic['topic'].replace(' ', '-'), topic['language'])
                headlines = self.get_by_topic(
                    topic['topic'],
                    language=topic['language'],
                    sort_by=_sort_by,
                    domains=topic['domains'],
                )
                dict_updater(topic_name, headlines)
            elif ('domains' in topic.keys()) and topic['domains']:
                topic_name = '%s' % (topic['topic'].replace(' ', '-'))
                headlines = self.get_by_topic(
                    topic['topic'],
                    sort_by=_sort_by,
                    domains=topic['domains'],
                )
                dict_updater(topic_name, headlines)
            elif ('language' in topic.keys()) and topic['language']:
                topic_name = '%s(%s)' % (topic['topic'].replace(' ', '-'), topic['language'])
                headlines = self.get_by_topic(
                    topic['topic'],
                    language=topic['language'],
                    sort_by=_sort_by,
                )
                dict_updater(topic_name, headlines)
            else:
                headlines = self.get_by_topic(
                        topic['topic'],
                        sort_by=_sort_by,
                    )
                dict_updater(topic['topic'].replace(' ', '-'), headlines)

        if self.with_threading:
            threads = []
            for topic in topics:
                t = threading.Thread(target=send_request, args=[topic])
                t.daemon = True
                t.start()
                threads.append(t)

            for t in threads:
                t.join()
        else:
            for topic in topics:
                send_request(topic)

        if sorted_by_date:
            for k, v in grouped_by_topic.items():
                grouped_by_topic[k] = self._sort_by_date(v, limit_by_date=False)
        for k, v in grouped_by_topic.items():
            grouped_by_topic[k] = self._cleanup_duplicates(v)
        if limit_at:
            for k, v in grouped_by_topic.items():
                if v: grouped_by_topic[k] = v[:limit_at]
        return grouped_by_topic


# Setup Vim
def buffer_setup():
    settings = (
        'noswapfile', 'buftype=nofile',
        'nonumber', 'cc=', 'conceallevel=1',
        'concealcursor=n'
    )
    for setting in settings:
        vim.command('setlocal ' + setting)
    for token in ('(', ')', '-', '$'):
        vim.command('setlocal isk+=%s' % token)


# Print to Vim
def write_to_buff(s):
    """Write string to current buffer."""
    current_buff = vim.current.buffer

    # buffer.append() cannot accept unicode type,
    # must first encode to UTF-8 string
    if isinstance(s, unicode):
        s = s.encode('utf-8', errors='replace')
    else:
        current_buff.append(s)


# Setup boundaries
def first_group_ends_at(menu, title):
    for l, c in enumerate(menu):
        found = c.lower().find(title.lower())
        if found != -1:
            loc = (l + 1, found + len(title) + 1)
            return loc


# Setup folding
def set_folds():
    """Save into SOURCE_FOLDS the line numbers where folds can be created."""
    for ln_nr, ln in enumerate(vim.current.buffer):
        if ln.startswith(SOURCE_EMPHASIS):
            source = ln.strip().strip(SOURCE_EMPHASIS).strip()
            SOURCE_FOLDS[source] = ln_nr + 1
    SOURCE_FOLDS['<EOF>'] = int(vim.eval('line("$")'))


# Do folding
def fold():
    def try_range(folds, line_nr):
        start, end = folds[0], folds[1] - 1
        if len(folds) == 2:
            return start, end
        if folds[0] <= line_nr <= folds[1]:
            return start, end
        return try_range(folds[1:], line_nr)

    current_line_nr, _ = vim.current.window.cursor
    start, stop = try_range(list(SOURCE_FOLDS.values()), current_line_nr + 1)

    if int(vim.eval('foldlevel(%s)' % (current_line_nr + 1))) > 0:
        try:
            vim.command('normal za')
        except vim.error as e:
            print('WARNING: %s' % e)
    else:
        vim.command('%s,%sfold' % (start, stop))


# Open an entry in a browser

# get an entry's url from the listing
def get_url(line_nr):
    line_number = line_nr - 1
    line = vim.current.buffer[line_number]
    if not line:
        return
    if line.startswith('[http'):
        inc = 1
        while True:
            if line[-1] == ']':
                return line[1:-1]
            line += vim.current.buffer[line_number + inc]
            inc += 1
    return get_url(line_nr + 1)

# open an entry in a default browser
def open_link():
    current_line_nr, _ = vim.current.window.cursor
    url = get_url(current_line_nr)

    if url:
        subprocess.call(['open', url])

# open an entry in the reader script within Vim
def open_link_inside():
    current_line_nr, _ = vim.current.window.cursor
    url = get_url(current_line_nr)

    if url:
        vim.command("new +set\ ft=news-headlines-preview")
        vim.command("read !reader '" + str(url) + "'")
        vim.command("normal! gg")


# Show all sources
def show_sources():
    """Print all possible news sources."""
    news = GetNews(API_KEY)
    sources = list(map(lambda x: str(x['id']), news.get_sources()))
    print(sources)


# Navigation
def go_next_article():
    vim.command("let search_org = @/")
    vim.command(r"let @/ = ')\n> '")
    vim.command("normal! nj^mh")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! N")
    vim.command("let source_name = getline('.')")
    vim.command("normal! 'hzz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_prev_article():
    vim.command("let search_org = @/")
    vim.command(r"let @/ = ')\n> '")
    vim.command("normal! NNj^mh")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! N")
    vim.command("let source_name = getline('.')")
    vim.command("normal! 'hzz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_next_source():
    vim.command("let search_org = @/")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! n")
    vim.command("let source_name = getline('.')")
    vim.command("normal! ^zz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
def go_prev_source():
    vim.command("let search_org = @/")
    vim.command("let @/ = '^⁕⁕⁕'")
    vim.command("normal! N")
    vim.command("let source_name = getline('.')")
    vim.command("normal! ^zz")
    vim.command("redraw")
    vim.command("echo source_name")
    vim.command("let @/ = search_org")
# currently unused, see below
# def go_source():
#     vim.command("let search_org = @/")
#     vim.command("let @/ = '⁕⁕⁕ ' . expand('<cWORD>') . ' ⁕⁕⁕'")
#     vim.command("normal! n")
#     vim.command("let source_name = getline('.')")
#     vim.command("normal! jj^zz")
#     vim.command("redraw")
#     vim.command("echo source_name")
#     vim.command("let @/ = search_org")
# go to a particular source in a buffer
def go_to_source():
    word_under_cursor = vim.eval('expand("<cWORD>")')
    if word_under_cursor in SOURCE_FOLDS.keys():
        target_line = str(SOURCE_FOLDS[word_under_cursor])
        vim.command(target_line)
        vim.command('normal zt')
        vim.command("redraw")
        vim.command('echo getline(".")')


def main():
    buffer_setup()

    # user customizable vars
    wrap_text = int(vim.eval('g:news_headlines_wrap_text'))
    show_published_at = int(vim.eval('g:news_headlines_show_published_at'))
    sources = vim.eval('g:news_headlines_sources')
    topics = vim.eval('g:news_headlines_topics')

    if not sources and not topics:
        write_to_buff('Nothing to show, check your g:news_headlines_... vars.')
        return

    html = HTMLParser()
    news = GetNews(API_KEY, with_threading=True)

    # don't want to sort sources (they go as declared in the config)
    if sources: GROUPS.append(news.group_by_source(sources))
    # but do want to sort topics (because multithreading messes them up)
    if topics:
        # introducing intermediate dicts to preserve the sorting as declared in the config
        groups_a = news.group_by_topic(topics)
        groups_b = collections.OrderedDict(sorted(groups_a.items()))
        # adding collected (and sorted according to the config) topics to the resulting list
        GROUPS.append(groups_b)

    wrapper = textwrap.TextWrapper(width=wrap_text)
    menu_wrapper = textwrap.TextWrapper(
        width=wrap_text, break_on_hyphens=False,
        break_long_words=False
    )
    title_wrapper = textwrap.TextWrapper(
        width=wrap_text, initial_indent=TITLE_EMPHASIS,
        subsequent_indent=TITLE_EMPHASIS
    )

    def write_content(groups):
        for group in groups:
            for title, headlines in group.items():
                write_to_buff(
                    '%s %s %s' % (
                        SOURCE_EMPHASIS,
                        title.upper(),
                        SOURCE_EMPHASIS
                    )
                )
                write_to_buff('')
                if not headlines:
                    continue
                for h in headlines:
                    if show_published_at and h['publishedAt']:
                        published_at = '(%s)' % re.sub(r'(.*)T(.*)Z', r'\1 \2', h['publishedAt'])
                    else:
                        published_at = ''
#                     write_to_buff(title_wrapper.wrap(html.unescape(h['title'] + published_at)))
                    if published_at:
                        write_to_buff(str(published_at))
                    write_to_buff(title_wrapper.wrap(html.unescape(h['title'])))
                    if h['description']:
                        write_to_buff(
                            wrapper.wrap(html.unescape(h['description'].lstrip(TITLE_EMPHASIS)))
                        )
                    write_to_buff(wrapper.wrap('[' + html.unescape(h['url'] + ']')))
                    write_to_buff('')

    def write_menu(groups, menu):
        to_write = []
        for group in groups:
            to_write.append('   '.join(map(lambda x: x.upper(), group.keys())))
        menu.extend(menu_wrapper.wrap('   '.join(to_write)))
        write_to_buff(menu)
        write_to_buff('')

    def set_buffer_vars(groups, menu):
        """Set wars used for syntax highlighting."""
        t_start_l = 1
        t_start_c = 1

        if sources:
            s_end = first_group_ends_at(menu, list(groups[0].keys())[-1])
            if s_end:
                t_start_l = s_end[0]
                t_start_c = s_end[1] + 1
        vim.command('let b:menu_topics_start_l=%s' % t_start_l)
        vim.command('let b:menu_topics_start_c=%s' % t_start_c)
        vim.command('let b:menu_until_l=%s' % (len(menu) + 1))

    write_menu(GROUPS, MENU)
    write_content(GROUPS)
    set_buffer_vars(GROUPS, MENU)

    del vim.current.buffer[0]
    set_folds()
