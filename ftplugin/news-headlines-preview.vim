" Local settings
" hide line numbers
setlocal norelativenumber
setlocal nonumber

" Local mappings
" custom Twitter poster
function! SendToTwit()
    let the_tit = ''
    let the_url = ''
    for i in range(1,10)
        if getline(i)[0:4] == '> htt'
            let the_url = getline(i)[2:]
        endif
        if getline(i)[0:1] == '# '
            let the_tit = getline(i)[2:]
        endif
        if the_tit != '' && the_url != ''
"             return the_tit . ' ' . the_url
            silent! execute '!open https://twitter.com/intent/tweet?text="' . the_tit . ' ' . the_url . '"'
            return
        endif
    endfor
    echohl WarningMsg
    echom "No related header found in this buffer"
    echohl None
endfunction
nnoremap <buffer> <silent> s :call SendToTwit()<CR>
