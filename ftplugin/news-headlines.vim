if has('python')
    command! -nargs=1 Python python <args>
elseif has('python3')
    command! -nargs=1 Python python3 <args>
else
    echo "NewsHeadlines.vim ERROR: Requires Vim compiled with +python or +python3"
    finish
endif

" Import Python code
execute "Python import sys"
execute "Python sys.path.append(r'" . expand("<sfile>:p:h") . "')"

Python << EOF
if 'news_headlines' not in sys.modules:
    import news_headlines
else:
    import imp
    # Reload python module to avoid errors when updating plugin
    news_headlines = imp.reload(news_headlines)
EOF

" Load front page
execute "Python news_headlines.main()"

" Local settings
" hide line numbers
setlocal norelativenumber
setlocal nonumber
" autocommands to turn Limelight on and off
function! LimelightOn()
    try
        Limelight 0.7
    catch /^Vim\%((\a\+)\)\=:E492/
    endtry
endfunction
autocmd! BufEnter,WinEnter,TabEnter <buffer> call LimelightOn()
function! LimelightOff()
    try
        Limelight!
    catch /^Vim\%((\a\+)\)\=:E492/
    endtry
endfunction
autocmd! BufLeave,BufDelete <buffer> call LimelightOff()

" Local mappings
" show sources
noremap <silent> <buffer> s :Python news_headlines.show_sources()<cr>
" go to a source in a buffer
noremap <silent> <buffer> <CR> :Python news_headlines.go_to_source()<cr>zz
" go top
nnoremap <silent> <buffer> gg :echo ""<CR>gg
" next / previous article
nnoremap <silent> <buffer> j :Python news_headlines.go_next_article()<CR>
nnoremap <silent> <buffer> k :Python news_headlines.go_prev_article()<CR>
" next / previous source
nnoremap <silent> <buffer> J :Python news_headlines.go_next_source()<CR>
nnoremap <silent> <buffer> K :Python news_headlines.go_prev_source()<CR>
" others
noremap <silent> <buffer> o :Python news_headlines.open_link()<cr>
noremap <silent> <buffer> p :Python news_headlines.open_link_inside()<cr>
noremap <silent> <buffer> f :Python news_headlines.fold()<cr>
